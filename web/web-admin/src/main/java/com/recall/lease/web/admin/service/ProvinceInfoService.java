package com.recall.lease.web.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.recall.lease.model.entity.ProvinceInfo;

/**
* @author liubo
* @description 针对表【province_info】的数据库操作Service
* @createDate 2023-07-24 15:48:00
*/
public interface ProvinceInfoService extends IService<ProvinceInfo> {

}

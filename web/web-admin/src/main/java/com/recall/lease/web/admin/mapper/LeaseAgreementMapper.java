package com.recall.lease.web.admin.mapper;

import com.recall.lease.model.entity.LeaseAgreement;
import com.recall.lease.web.admin.vo.agreement.AgreementQueryVo;
import com.recall.lease.web.admin.vo.agreement.AgreementVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author liubo
* @description 针对表【lease_agreement(租约信息表)】的数据库操作Mapper
* @createDate 2023-07-24 15:48:00
* @Entity com.atguigu.lease.model.LeaseAgreement
*/
public interface LeaseAgreementMapper extends BaseMapper<LeaseAgreement> {

}





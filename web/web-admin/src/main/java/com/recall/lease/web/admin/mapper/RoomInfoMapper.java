package com.recall.lease.web.admin.mapper;

import com.recall.lease.model.entity.RoomInfo;
import com.recall.lease.web.admin.vo.room.RoomItemVo;
import com.recall.lease.web.admin.vo.room.RoomQueryVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author liubo
* @description 针对表【room_info(房间信息表)】的数据库操作Mapper
* @createDate 2023-07-24 15:48:00
* @Entity com.atguigu.lease.model.RoomInfo
*/
public interface RoomInfoMapper extends BaseMapper<RoomInfo> {

}





package com.recall.lease.model.enums;

public interface BaseEnum {

    Integer getCode();

    String getName();
}
